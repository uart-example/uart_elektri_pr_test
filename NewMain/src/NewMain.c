/*
 * NewMain.c
 *
 *  Created on: Apr 14, 2024
 *      Author: krist
 */
#include "main.h"
#include "usart.h"
#include <string.h>

uint8_t data[1] = {0};
char rnd[] = "\r\n ";
char works[] = "works";
uint16_t lenOfRxData = 1;

void APP_NewMain()
{

	HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin,1);
	HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin,1);
	HAL_GPIO_WritePin(L3_GPIO_Port, L3_Pin,1);
	HAL_GPIO_WritePin(L4_GPIO_Port, L4_Pin,1);
	HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,1);
	HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,1);
	/*Receive data from the from the users console*/
	HAL_UART_Receive_IT(&huart2, data, lenOfRxData);

	while(1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
 /* uint8_t* data2 = &data[0];
  uint8_t* data3 = &data[1];
  HAL_UART_Transmit(huart, (uint8_t*)rnd,strlen(rnd), 1000);
  HAL_UART_Transmit(huart, data2,1, 1000);
  HAL_UART_Transmit(huart, (uint8_t*)rnd,strlen(rnd), 1000);
  HAL_UART_Transmit(huart, data3,1, 1000);
  HAL_UART_Transmit(huart, (uint8_t*)rnd,strlen(rnd), 1000);*/
  HAL_UART_Transmit(huart, (uint8_t*)rnd,strlen(rnd), 1000);
  if(data[0] == 49)//1
  {

	  HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin,GPIO_PIN_RESET);//Light on

  }
  else if(data[0] == 50)//2
  {
	  HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin,GPIO_PIN_SET);//Light off
  }
  else if(data[0] == 51)//3
  {
	  HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin,GPIO_PIN_RESET);//Light ON
  }
  else if(data[0] == 52)//4
  {
	  HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin,GPIO_PIN_SET);//Light off
  }
  else if(data[0] == 53)//5
  {
	  HAL_GPIO_WritePin(L3_GPIO_Port, L3_Pin,GPIO_PIN_RESET);//Light on
  }
  else if(data[0] == 54)//6
  {
	  HAL_GPIO_WritePin(L3_GPIO_Port, L3_Pin,GPIO_PIN_SET);//Light Off
  }
  else if(data[0] == 55)//7
  {
	  HAL_GPIO_WritePin(L4_GPIO_Port, L4_Pin,GPIO_PIN_RESET);//Light On
  }
  else if(data[0] == 56)//8
  {
	  HAL_GPIO_WritePin(L4_GPIO_Port, L4_Pin,GPIO_PIN_SET);//Light Off
  }
  else if(data[0] == 57)//9
  {
	  HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_RESET);//Light On
  }
  else if(data[0] == 65 || data[0]==97)//A
  {
	  HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);//Light Off
  }
  else if(data[0]==65|| data[0] == 98)//B
  {
	  HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_RESET);//Light On
  }
  else if (data[0] == 67 || data[0] == 99)
  {
	  HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_SET);//Light Off
  }
  HAL_UART_Receive_IT(&huart2, data,lenOfRxData);



}
